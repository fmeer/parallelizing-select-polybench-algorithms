/*
This code is part of the project "Parallelizing Select Polybench Algorithms" for 
the course "Design of Parallel High Performance Computing" at ETH Zürich in HS2020
*/
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include <float.h>
#include <sys/time.h>

void print_mat(int ni, int nj, double A[ni][nj])
{
    for (int r = 0; r < ni; ++r)
    {
        for (int c = 0; c < nj; ++c)
            printf("%f ", A[r][c]);
        printf("\n");
    }
    //printf("\n");
}

int main(int argc, char **argv)
{
    int rank, size;
    const int ni = 10000; //Number of random variables
    const int nj = 10000; //Number of data points per random variable
    double stddevx = 0.;
    double stddevy = 0.;
    int count = 0;
    double mean;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    struct timeval stop, start;
    gettimeofday(&start, NULL);

    int iterations = ni / size;
    int rest = ni % size;

    if (rank < rest)
    {
        iterations++;
    }

    double local[iterations][nj];
    double correlation_result[iterations][ni]; //Correlation Matrix is Square
    memset(correlation_result, 0, iterations * ni * sizeof(double));
    double tmp[iterations][nj];
    //Initializer Polybench 4.2
    for (int i = 0; i < iterations; i++)
    {
        for (int j = 0; j < nj; j++)
        {
            local[i][j] = (((double)(((i + iterations * rank) * j) % nj) / nj) * 100) + 10;
        }
    }

    for (int i = 0; i < iterations; i++)
    {
        mean = 0.;
        for (int j = 0; j < nj; j++)
        {
            mean += local[i][j];
        }
        mean /= nj;

        for (int j = 0; j < nj; j++)
        {
            local[i][j] -= mean;
        }
    }

    //Kernel Start
    for (int i = 0; i < size; i++)
    {
        if (rank == i)
        {
            if (rank != size - 1)
            {
                MPI_Bcast(&local, nj * iterations, MPI_DOUBLE, rank, MPI_COMM_WORLD);
            }
            for (int j = 0; j < iterations; j++)
            {
                for (int k = j; k < iterations; k++)
                {
                    for (int l = 0; l < nj; l++)
                    {
                        correlation_result[k][j + rank * (iterations)] += local[j][l] * local[k][l];
                    }
                    correlation_result[k][j + rank * (iterations)] /= ni - 1;
                }
            }
            gettimeofday(&stop, NULL);
        }
        else
        {
            if (i != size - 1)
            {
                MPI_Bcast(tmp, nj * iterations, MPI_DOUBLE, i, MPI_COMM_WORLD);
            }
            if (rank > i)
            {

                for (int j = 0; j < iterations; j++)
                {
                    for (int k = 0; k < iterations; k++)
                    {
                        for (int l = 0; l < nj; l++)
                        {
                            correlation_result[k][j + i * (iterations)] += tmp[j][l] * local[k][l];
                        }
                        correlation_result[k][j + i * (iterations)] /= ni - 1;
                    }
                }
            }
        }
    }
    //Kernel finished

    if (rank != 0)
    {
        for (int i = 0; i < iterations; i++)
        {
            MPI_Send(&correlation_result[i][0], ni, MPI_DOUBLE, 0, i + rank * iterations, MPI_COMM_WORLD);
        }
    }

    if (rank == 0)
    {
        double result[ni][ni];
        //Write Local Results to Global Result Matrix
        for (int i = 0; i < iterations; i++)
        {
            for (int k = 0; k < ni; k++)
            {
                result[i][k] = correlation_result[i][k];
            }
        }
        //Receive/Write Local Results from other processors to Global Result Matrix
        int ok = 1;
        int cnt = 0;
        for (int j = iterations; j < ni; j++)
        {
            MPI_Recv(&result[j][0], ni, MPI_DOUBLE, ok, j, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            cnt++;
            if (cnt == iterations)
            {
                ok++;
                cnt = 0;
            }
        }

        //Make Symmetric
        for (int i = 0; i < ni; i++)
        {
            for (int j = i; j < ni; j++)
            {
                if (i != j)
                    result[i][j] = result[j][i];
            }
        }

        // print_mat(ni,ni,result);
    }

    //Get Time For All Processors
    printf("rank %d took %f s\n", rank, (double)((stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec) / 1000000);

    MPI_Finalize();
    return 0;
}
