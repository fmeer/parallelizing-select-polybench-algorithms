# Parallelizing Select Polybench Algorithms

## Abstract
In this paper we aim to parallelize four PolyBench algorithms, namely: Correlation Matrix, Covariance Matrix, Gram-Schmidt Process and the LU Decomposition. For each of the four algorithms we present three different parallel versions: Message Passing Interface(MPI), Open Multi-Processing (OpenMP), and a version which includes both MPI and Open MP (Hybrid).




## Compilation
All source files are located within their respective folders and were be compiled using the following commands.

#### MPI
Compiled with
```sh
mpicc -lm -O3 
```

#### OpenMP
Compiled with
```sh
gcc -lm -O3 -fopenmp
```

#### Hybrid
Compiled with
```sh
mpicc -lm -O3 -fopenmp
```


## References
All original PolyBench/C 3.2 source codes can be found under the following link
* https://web.cse.ohio-state.edu/~pouchet.2/software/polybench/
