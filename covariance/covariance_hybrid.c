/*
This code is part of the project "Parallelizing Select Polybench Algorithms" for 
the course "Design of Parallel High Performance Computing" at ETH Zürich in HS2020
*/
#include <mpi.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/time.h>
#include <float.h>
#include <omp.h>

void print_mat(int ni, int nj, double A[ni][nj])
{
    for (int r = 0; r < ni; ++r)
    {
        for (int c = 0; c < nj; ++c)
            printf("%f ", A[r][c]);
            printf("\n");
    }
    printf("\n");
}

int main(int argc, char **argv)
{
    struct timeval stop, start;
    gettimeofday(&start, NULL);
    int rank, size;
    const int ni = 10000; //Number of random variables
    const int nj = 10000; //Number of data points per random variable
    double mean;
    int count = 0;
    
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    int iterations = ni / size;
    int rest = ni % size;

    if (rank < rest)
    {
        iterations++;
    }

    double local[iterations][nj];
    double covariance_result[iterations][ni]; //covar mat is quadratic
    memset(covariance_result, 0, iterations * ni * sizeof(double));
    double tmp[nj];

    //Initializer Polybench 4.2
    #pragma omp parallel for
    for (int i = 0; i < iterations; i++)
    {
        for (int j = 0; j < nj; j++)
        {
            local[i][j] = (((double)(((i * size + rank) * j) % nj) / nj) * 100) + 10;
        }
    }
    
//Kernel start
    #pragma omp parallel for reduction(+:mean)
    for (int i = 0; i < iterations; i++)
    {
         mean = 0.;
        for (int j = 0; j < nj; j++)
        {
            mean += local[i][j];
        }
        mean /= nj;

        for (int j = 0; j < nj; j++)
        {
            local[i][j] -= mean;
        }
    }
    
    for (int j = 0; j < ni; j++)
    {
        if (rank == j % size)
        {
            MPI_Bcast(&local[count][0], nj, MPI_DOUBLE, rank, MPI_COMM_WORLD);
            #pragma omp parallel for
            for (int i = count; i < iterations; i++)
            {
                for (int k = 0; k < nj; k++)
                {
                    covariance_result[i][j] += local[count][k] * local[i][k];
                }
                covariance_result[i][j] /= nj - 1;
            }
            ++count;
        }
        else
        {
            MPI_Bcast(tmp, nj, MPI_DOUBLE, j % size, MPI_COMM_WORLD);
            #pragma omp parallel for
            for (int i = count; i < iterations; i++)
            {
                for (int k = 0; k < nj; k++)
                {
                    covariance_result[i][j] += tmp[k] * local[i][k];
                }
                covariance_result[i][j] /= nj - 1;
            }
        }
    }
    //Kernel finished

    if (rank != 0)
    {
        for (int i = 0; i < iterations; i++)
        {
            MPI_Send(&covariance_result[i][0], ni, MPI_DOUBLE, 0, i * size + rank, MPI_COMM_WORLD);
        }
    }

    if (rank == 0)
    {
        double result[ni][ni];
        for (int i = 0; i < iterations; i++)
        {
            for (int k = 0; k < ni; k++)
            {
                result[i * size][k] = covariance_result[i][k];
            }
        }

        for (int j = 0; j < ni; j++)
        {
            if (rank != j % size)
                MPI_Recv(&result[j][0], ni, MPI_DOUBLE, j % size, j, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        }
        //Make Symmetric
        #pragma omp parallel for
        for (int i = 0; i < ni; i++)
        {
            for (int j = i; j < ni; j++)
            {
                if (i != j)
                    result[i][j] = result[j][i];
            }
        }
        //print_mat(ni,ni,result);
    }

    gettimeofday(&stop, NULL);
    printf("took %f s\n", (double)((stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec) / 1000000);

    MPI_Finalize();
    return 0;
}