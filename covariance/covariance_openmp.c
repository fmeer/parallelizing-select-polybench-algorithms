/*
This code is part of the project "Parallelizing Select Polybench Algorithms" for 
the course "Design of Parallel High Performance Computing" at ETH Zürich in HS2020
*/
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/time.h>
#include <float.h>
#include <omp.h>

void print_mat(int ni, int nj, double A[ni][nj])
{
    for (int r = 0; r < ni; ++r)
    {
        for (int c = 0; c < nj; ++c)
            printf("%f ", A[r][c]);
            printf("\n");
    }
    printf("\n");
}

int main(int argc, char **argv)
{
    struct timeval stop, start;
    gettimeofday(&start, NULL);
    int ni = 10000; //Number of random variables
    int nj = 10000; //Number of data points per random variable
    double A[ni][nj];
    double covariance_result[ni][ni];
    double mean;
    memset(covariance_result, 0, ni * ni * sizeof(double));

//Initializer Polybench 4.2
#pragma omp parallel for
    for (int i = 0; i < ni; i++)
    {
        for (int j = 0; j < nj; j++)
        {
            A[i][j] = (((double) ((i*j) % nj) / nj )*100) + 10;
        }
    }

//Kernel start
#pragma omp parallel for reduction(+:mean)
    for (int i = 0; i < ni; i++)
    {
        mean = 0.;
        for (int j = 0; j < nj; j++)
        {
            mean += A[i][j];
        }
        mean /= nj;

        for (int j = 0; j < nj; j++)
        {
            A[i][j] -= mean;
        }
    }

#pragma omp parallel for
    for (int j = 0; j < ni; j++)
    {
        for (int i = j; i < ni; i++)
        {
                for (int k = 0; k < nj; k++)
                {
                    covariance_result[j][i] += A[j][k] * A[i][k];
                }
                covariance_result[j][i] /= nj-1;
                covariance_result[i][j] = covariance_result[j][i];
        }
    }
//Kernel finish

    //print_mat(ni,ni,covariance_result);

    gettimeofday(&stop, NULL);
    printf("took %f s\n", (double)((stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec) / 1000000);
    
    return 0;
}