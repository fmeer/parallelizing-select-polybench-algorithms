/*
This code is part of the project "Parallelizing Select Polybench Algorithms" for 
the course "Design of Parallel High Performance Computing" at ETH Zürich in HS2020
*/
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <float.h>
#include <sys/time.h>
#include <omp.h>

void print_mat(int ni, int nj, double A[ni][nj])
{
    for (int r = 0; r < ni; ++r)
    {
        for (int c = 0; c < ni; ++c)
            printf("%f ", A[r][c]);
        printf("\n");
    }
    printf("\n");
}

int main(int argc, char **argv)
{
    struct timeval stop, start;
    gettimeofday(&start, NULL);
    const int n = 10000; // Matrix size
    double A[n][n];
    double x;

// Polybench 4.2 initializer
#pragma omp parallel for
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j <= i; j++)
            A[i][j] = (double)(-j % n) / n + 1;
        for (int j = i + 1; j < n; j++)
        {
            A[i][j] = 0;
        }
        A[i][i] = 1;
    }
    
// Kernel start
    for (int j = 0; j < n; j++)
    {
        #pragma omp parallel for private(x)
        for (int i = j + 1; i < n; i++)
        {
            x = A[i][j] / A[j][j];

            for (int k = j; k < n; k++)
            {
                A[i][k] -= x * A[j][k];
            }
            A[i][j] = x;
        }
    }
// Kernel finish

    //print_mat(n, n, A);

    gettimeofday(&stop, NULL);
    printf("took %f s\n", (double)((stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec) / 1000000);

    return 0;
}
