/*
This code is part of the project "Parallelizing Select Polybench Algorithms" for 
the course "Design of Parallel High Performance Computing" at ETH Zürich in HS2020
*/
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include <float.h>
#include <sys/time.h>

void print_mat(int ni, int nj, double A[ni][nj])
{
    for (int r = 0; r < ni; ++r)
    {
        for (int c = 0; c < nj; ++c)
            printf("%f ", A[r][c]);
            printf("\n");
    }
    printf("\n");
}

int main(int argc, char **argv)
{
    struct timeval stop, start;
    gettimeofday(&start, NULL);
    int rank, size;
    const int n = 10000; // Matrix size
    int count = 0;
    double x;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    int iterations = n / size;
    int rest = n % size;

    if (rank < rest)
    {
        iterations++;
    }

    double local[iterations][n];
    double tmp[n];

    //Initializer Polybench 4.2
     for (int i = 0; i < iterations; i++)
    {
        for (int j = 0; j <= i*size+rank; j++)
            local[i][j] = (double)(-j % n) / n + 1;
        for (int j = (i*size+rank) + 1; j < n; j++)
        {
            local[i][j] = 0;
        }
        local[i][(i*size+rank)] = 1;
    }
    // Kernel start
    for (int j = 0; j < n; j++)
    {
        if (rank == j % size)
        {
            MPI_Bcast(&local[count][0], n, MPI_DOUBLE, rank, MPI_COMM_WORLD);
            for (int i = count + 1; i < iterations; i++)
            {
                x = local[i][j] / local[count][j];
                for (int k=j; k < n; k++)
                {
                    local[i][k] -= x * local[count][k];
                }
                local[i][j] = x;
            }
            count++;
        }
        else
        {
            MPI_Bcast(tmp, n, MPI_DOUBLE, j % size, MPI_COMM_WORLD);
            for (int i = count; i < iterations; i++)
            {
                x = local[i][j] / tmp[j];
                for (int k=j; k < n; k++)
                {
                    local[i][k] -= x * tmp[k];
                }
                local[i][j] = x;
            }
        }
    }
    // Kernel finish
    if (rank != 0)
    {
        for (int i = 0; i < iterations; i++)
        {
            MPI_Send(&local[i][0], n, MPI_DOUBLE, 0, i * size + rank, MPI_COMM_WORLD);
        }
    }

    if (rank == 0)
    {
        double result[n][n];
        for (int i = 0; i < iterations; i++)
        {
            for (int k = 0; k < n; k++)
            {
                result[i * size][k] = local[i][k];
            }
        }

        for (int j = 0; j < n; j++)
        {
            if (rank != j % size)
                MPI_Recv(&result[j][0], n, MPI_DOUBLE, j % size, j, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        }

        //print_mat(n,n,result);
    }

    gettimeofday(&stop, NULL);
    printf("took %f s\n", (double)((stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec) / 1000000);

    MPI_Finalize();
    return 0;
}