/*
This code is part of the project "Parallelizing Select Polybench Algorithms" for 
the course "Design of Parallel High Performance Computing" at ETH Zürich in HS2020
*/
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include <float.h>
#include <sys/time.h>
#include <time.h>
#include <stdlib.h>
#include <omp.h>

int check_if_zero_vector(int nj, int iterations, int count, double local[iterations][nj])
{
  for (int i = 0; i < nj; i++)
  {
    if (local[count][i] > FLT_EPSILON || local[count][i] < -FLT_EPSILON)
      return 0;
  }
  return 1;
}

void print_mat(int ni, int nj, double A[ni][nj])
{
    for (int r = 0; r < ni; ++r)
    {
        for (int c = 0; c < nj; ++c)
            printf("%f ", A[r][c]);
        printf("\n");
    }
    printf("\n");
}

int main(int argc, char **argv)
{
  struct timeval stop, start;
  gettimeofday(&start, NULL);
  int rank, size;
  const int ni = 10000; //Number of random variables
  const int nj = 10000; //Number of data points per random variable
  double proj_value;

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  int count = 0;
  int iterations = ni / size;
  int rest = ni % size;

  if (rank < rest)
  {
    iterations++;
  }

  double local[iterations][nj];
  double projection_result[iterations][nj];
  memset(projection_result, 0, (nj)*iterations * sizeof(double));
  double tmp[nj];

//Initializer Polybench 4.2
#pragma omp parallel for
  for (int i = 0; i < iterations; i++)
  {
    for (int j = 0; j < nj; j++)
    {
      local[i][j] = (((double)(((i * size + rank) * j) % nj) / nj) * 100) + 10;
    }
  }

//Kernel start
  for (int j = 0; j < ni; j++)
  {
    if (rank == j % size)
    {
      //Send Function
      #pragma omp parallel for
      for (int i = 0; i < nj; i++)
      {
        local[count][i] -= projection_result[count][i];
      }
      if (check_if_zero_vector(nj, iterations, count, local) == 0)
      {
        double norm = 0.;
        #pragma omp parallel for reduction(+:norm)
        for (int i = 0; i < nj; i++)
        {
          norm += local[count][i] * local[count][i];
        }
        norm = sqrt(norm);
        #pragma omp parallel for 
        for (int i = 0; i < nj; i++)
        {
          local[count][i] /= norm;
        }
      }

      MPI_Bcast(&local[count][0], nj, MPI_DOUBLE, rank, MPI_COMM_WORLD);
      #pragma omp parallel for reduction(+:proj_value)
      for (int i = count + 1; i < iterations; i++)
      {
        proj_value = 0.;

        for (int j = 0; j < nj; j++)
          proj_value += local[count][j] * local[i][j];

        for (int j = 0; j < nj; j++)
        {
          projection_result[i][j] += local[count][j] * proj_value;
        }
      }
      count++;
    }
    else
    {
      MPI_Bcast(tmp, nj, MPI_DOUBLE, j % size, MPI_COMM_WORLD);
      #pragma omp parallel for reduction(+:proj_value)
      for (int i = count; i < iterations; i++)
      {
        proj_value = 0.;

        for (int j = 0; j < nj; j++) // <u,v>/<u,u> since u is already normalized
          proj_value += tmp[j] * local[i][j];

        for (int j = 0; j < nj; j++)
        {
          projection_result[i][j] += tmp[j] * proj_value;
        }
      }
    }
  }
//Kernel finish

  if (rank != 0)
  {
    for (int i = 0; i < iterations; i++)
    {
      MPI_Send(&local[i][0], nj, MPI_DOUBLE, 0, i * size + rank, MPI_COMM_WORLD);
    }
  }
  if (rank == 0)
  {
    double result[ni][nj];
    for (int i = 0; i < iterations; i++)
    {
        for (int k = 0; k < nj; k++)
        {
          result[i * size][k] = local[i][k];
        }
    }

    for (int j = 0; j < ni; j++)
    {
      if (rank != j % size)
        MPI_Recv(&result[j][0], nj, MPI_DOUBLE, j % size, j, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }

    //print_mat(ni,nj,result);
  }

  gettimeofday(&stop, NULL);
  printf("took %f s\n", (double)((stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec) / 1000000);

  MPI_Finalize();
  return 0;
}
