/*
This code is part of the project "Parallelizing Select Polybench Algorithms" for 
the course "Design of Parallel High Performance Computing" at ETH Zürich in HS2020
*/
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <float.h>
#include <sys/time.h>
#include <omp.h>

void print_mat(int ni, int nj, double A[ni][nj])
{
    for (int r = 0; r < ni; ++r)
    {
        for (int c = 0; c < nj; ++c)
            printf("%f ", A[r][c]);
        printf("\n");
    }
    printf("\n");
}

int main(int argc, char **argv)
{
    struct timeval stop, start;
    gettimeofday(&start, NULL);
    const int ni = 10000; //Number of random variables
    const int nj = 10000; //Number of data points per random variable
    double norm;
    double proj_value;
    double A[ni][nj];

    #pragma omp parallel for 
    for (int i = 0; i < ni; i++)
    {
        for (int j = 0; j < nj; j++)
        {
            A[i][j] = (((double) ((i*j) % nj) / nj )*100) + 10;
        } 
    }
    
    //Kernel start
    for (int i = 0; i < ni; i++)
    {
        //Start Norm
        norm = 0.;
        #pragma omp parallel for reduction(+:norm)
        for (int j = 0; j < nj; j++)
        {
            norm += A[i][j] * A[i][j];
        }
        norm = sqrt(norm);
        if(norm>FLT_EPSILON){
        #pragma omp parallel for
        for (int j = 0; j < nj; j++)
        {
            A[i][j] /= norm;
        }
        }
        //End Norm
           #pragma omp parallel for reduction(+:proj_value)
        for (int k = i + 1; k < ni; k++)
        {
            proj_value = 0.;
            for (int j = 0; j < nj; j++) // <u,v>/<u,u> since u is already normalized
                proj_value += A[k][j] * A[i][j];
            for (int j = 0; j < nj; j++)
            {
                A[k][j] -= proj_value * A[i][j];
            }
        }
    }
    //Kernel finish
    //print_mat(ni, nj, A);

    gettimeofday(&stop, NULL);
    printf("took %f s\n", (double)((stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec) / 1000000);

    return 0;
}