/*
This code is part of the project "Parallelizing Select Polybench Algorithms" for 
the course "Design of Parallel High Performance Computing" at ETH Zürich in HS2020
*/
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/time.h>
#include <float.h>
#include <omp.h>

void print_mat(int ni, int nj, double A[ni][nj])
{
    for (int r = 0; r < ni; ++r)
    {
        for (int c = 0; c < nj; ++c)
            printf("%f ", A[r][c]);
            printf("\n");
    }
    printf("\n");
}

int main(int argc, char **argv)
{
    struct timeval stop, start;
    gettimeofday(&start, NULL);
    int ni = 10000; //Number of random variables
    int nj = 10000; //Number of data points per random variable
    double stddevx = 0.;
    double stddevy = 0.;
    double mean;
    double A[ni][nj];
    double correlation_result[ni][ni];
    memset(correlation_result, 0, ni * ni * sizeof(double));

    //Initializer Polybench 4.2
    #pragma omp parallel for
    for (int i = 0; i < ni; i++)
    {
        for (int j = 0; j < nj; j++)
        {
            A[i][j] = (((double)((i * j) % nj) / nj) * 100) + 10;
        }
    }
    
//Kernel start
    #pragma omp parallel for reduction(+: mean)
    for (int i = 0; i < ni; i++)
    {
        mean = 0.;
        for (int j = 0; j < nj; j++)
        {
            mean += A[i][j];
        }
        mean /= nj;

        for (int j = 0; j < nj; j++)
        {
            A[i][j] -= mean;
        }
    }
    
    #pragma omp parallel for reduction(+:stddevx, stddevy)
    for (int j = 0; j < ni; j++)
    {
        for (int i = j; i < ni; i++)
        {

            if (i != j)
            {
                for (int k = 0; k < nj; k++)
                {
                    correlation_result[j][i] += A[j][k] * A[i][k];
                    stddevx += A[j][k] * A[j][k];
                    stddevy += A[i][k] * A[i][k];
                }
                if (stddevx < FLT_EPSILON || stddevy < FLT_EPSILON)
                    stddevx = stddevy = 1.;
                correlation_result[j][i] /= sqrt(stddevx * stddevy);
                correlation_result[i][j] = correlation_result[j][i];
                stddevx = 0.;
                stddevy = 0.;
            }
            else
            {
                correlation_result[j][i] = 1.;
            }
        }
    }
    //Kernel finish
    //print_mat(ni,ni,correlation_result);
    

    gettimeofday(&stop, NULL);
    printf("took %f s\n", (double)((stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec) / 1000000);

    return 0;
}
