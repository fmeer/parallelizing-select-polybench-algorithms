/*
This code is part of the project "Parallelizing Select Polybench Algorithms" for 
the course "Design of Parallel High Performance Computing" at ETH Zürich in HS2020
*/
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include <float.h>
#include <sys/time.h>
#include <omp.h>

void print_mat(int ni, int nj, double A[ni][nj])
{
    for (int r = 0; r < ni; ++r)
    {
        for (int c = 0; c < nj; ++c)
            printf("%f ", A[r][c]);
            printf("\n");
    }
    printf("\n");
}

int main(int argc, char **argv)
{
    struct timeval stop, start;
    gettimeofday(&start, NULL);
    int rank, size;
    const int ni = 10000; //Number of random variables
    const int nj = 10000; //Number of data points per random variable
    double stddevx = 0.;
    double stddevy = 0.;
    int count = 0;
    double mean;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    int iterations = ni / size;
    int rest = ni % size;

    if (rank < rest)
    {
        iterations++;
    }

    double local[iterations][nj];
    double correlation_result[iterations][ni]; //corr mat is quadratic
    memset(correlation_result, 0, iterations * ni * sizeof(double));
    double tmp[nj];

    //Initializer Polybench 4.2
    #pragma omp parallel for
    for (int i = 0; i < iterations; i++)
    {
        for (int j = 0; j < nj; j++)
        {
            local[i][j] = (((double)(((i * size + rank) * j) % nj) / nj) * 100) + 10;
        }
    }

//Kernel start
    #pragma omp parallel for reduction(+:mean)
    for (int i = 0; i < iterations; i++)
    {
         mean = 0.;
        for (int j = 0; j < nj; j++)
        {
            mean += local[i][j];
        }
        mean /= nj;

        for (int j = 0; j < nj; j++)
        {
            local[i][j] -= mean;
        }
    }

    
    for (int j = 0; j < ni; j++)
    {
        if (rank == j % size)
        {
            MPI_Bcast(&local[count][0], nj, MPI_DOUBLE, rank, MPI_COMM_WORLD);
            #pragma omp parallel for reduction(+:stddevx,stddevy)
            for (int i = count; i < iterations; i++)
            {
                for (int k = 0; k < nj; k++)
                {
                    correlation_result[i][j] += local[count][k] * local[i][k];
                    stddevx += local[count][k] * local[count][k];
                    stddevy += local[i][k] * local[i][k];
                }
                if (stddevx < FLT_EPSILON || stddevy < FLT_EPSILON)
                    stddevx = stddevy = 1.;
                correlation_result[i][j] /= sqrt(stddevx * stddevy);
                stddevx = 0.;
                stddevy = 0.;
            }
            ++count;
        }
        else
        {
            MPI_Bcast(tmp, nj, MPI_DOUBLE, j % size, MPI_COMM_WORLD);
            #pragma omp parallel for reduction(+:stddevx,stddevy)
            for (int i = count; i < iterations; i++)
            {
                for (int k = 0; k < nj; k++)
                {
                    correlation_result[i][j] += tmp[k] * local[i][k];
                    stddevx += tmp[k] * tmp[k];
                    stddevy += local[i][k] * local[i][k];
                }
                if (stddevx < FLT_EPSILON || stddevy < FLT_EPSILON)
                    stddevx = stddevy = 1.;
                correlation_result[i][j] /= sqrt(stddevx * stddevy);
                stddevx = 0.;
                stddevy = 0.;
            }
        }
    }
//Kernel finished

    if (rank != 0)
    {
        for (int i = 0; i < iterations; i++)
        {
            MPI_Send(&correlation_result[i][0], ni, MPI_DOUBLE, 0, i * size + rank, MPI_COMM_WORLD);
        }
    }

    if (rank == 0)
    {
        double result[ni][ni];
        //Write Local Results to Global Result Matrix
        for (int i = 0; i < iterations; i++)
        {
            for (int k = 0; k < ni; k++)
            {
                result[i * size][k] = correlation_result[i][k];
            }
        }
        //Receive/Write Local Results from other processors to Global Result Matrix
        for (int j = 0; j < ni; j++)
        {
            if (rank != j % size)
                MPI_Recv(&result[j][0], ni, MPI_DOUBLE, j % size, j, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        }
        //Make Symmetric
        #pragma omp parallel for
        for (int i = 0; i < ni; i++)
        {
            result[i][i] = 1.;
            for (int j = i; j < ni; j++)
            {
                if (i != j)
                    result[i][j] = result[j][i];
            }
        }
        //print_mat(ni,ni,result);
    }
    
    //Get Time For All Processors
    gettimeofday(&stop, NULL);
    printf("took %f s\n", (double)((stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec) / 1000000);

    MPI_Finalize();
    return 0;
}